## Chris Holle 
## CSI 3130 
## Assignment 5  
# README


1.  See logger.log, why is it different from the log to console?
	* logger.log will post to a log file instead of posting logs in the console.  
1.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']  
    * This comes from one of the dependencies Maven inserted from junit-platform-engine.
1.  What does Assertions.assertThrows do?
    * It is a JUnit function that throws the exception passed in the parameters.
1.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it?  
     * If a serializable class does not explicitly declare a serialVersionUID, then the serialization runtime will calculate a default serialVersionUID 
       value for that class based on various aspects of the class. However, it is strongly recommended that all serializable classes explicitly declare serialVersionUID values, since the default serialVersionUID 
       computation is highly sensitive to class details that may vary depending on compiler implementations, and can thus result in unexpected InvalidClassExceptions during deserialization.
    2.  Why do we need to override constructors?
     * We override this exception so that we are able to pass a specific message through the exception which is useful for debugging.
    3.  Why we did not override other Exception methods?  
     * Because the other functions used are built-in and already provide the necessary information.
1.  The Timer.java has a static block static {}, what does it do?   
    * This static block uses the logger.properties to configure the logging.
1.  What is README.md file format how is it related to bitbucket?
    * README.md is a markdown file formate and is used to dislpay text in a given way. It is related to bitbucket,
    * in the way that when you create the README.md in the root, it becomes the overview page for the repository.
1.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)  
    * The test is failing because the Assertions.assertThrows is unable to throw an exception. We need to add a way to return the exception to the Tester.
1.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
    * Because it was expecting the exception to be explicitly thrown it would go through and cause errros. Now it is checked at runtime instead of compile time.
1.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
1.  Make a printScreen of your eclipse Maven test run, with console
1.  What category of Exceptions is TimerException and what is NullPointerException
    * TimerException is a checked exception. NullPointerException is raised when referring to the members of a null object and it is unchecked.
1.  Push the updated/fixed source code to your own repository.